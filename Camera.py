# import the opencv library
import cv2
class Camera:
  def __init__(self):
      # define a video capture object
      self.vid = cv2.VideoCapture(0)

  def GetFrame(self):
          # Capture the video frame
          # by frame
          ret, self.frame = self.vid.read()

          # Display the resulting frame
          cv2.imshow('frame', self.frame)
          if cv2.waitKey(1) & 0xFF == ord('q'):
              self.Destructor()
          return  self.frame

  def Destructor(self):
          # After the loop release the cap object
          self.vid.release()
          # Destroy all the windows
          cv2.destroyAllWindows()