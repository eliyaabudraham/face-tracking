<a id="readme-top"></a>

<!-- PROJECT LOGO -->
<br>
<div align="center">
  <a href="https://gitlab.com/Dan1451/ashdod-1002-eye-tracking.git">
  <img src="resources\logo.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Control the computer using your face</h3>

  <p align="center">
    <a href="https://gitlab.com/aurariolu2005/photomap-copy"><strong>Explore the docs »</strong></a>
    <br>
    <br>
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#installation">Installation</a>
      <ul>
        <li><a href="#dependencies">Dependencies</a></li>
        <li><a href="#finally">Finally</a></li>
      </ul>
    </li>
    <li><a href="#activation">Activation</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

## About The Project

This project is about controlling a computer with your face, we creat a machine lerning model<br> 
using YOLO (You Only Look Once) object detection, also we used COCO 
(Common Objects in Context) data set.<br> we take the outpot from the model and calculate the dest coordinates, and send them to the mouse.<br>

<p align="right">(<a href="#readme-top">back to top</a>)</p>

### Built With

* [![Tensorflow][Tensorflow.py]][Tensorflow-url]
* [![opencv][opencv.py]][opencv-url]
* [![python][python.py]][python-url]
* [![numpy][numpy.py]][numpy-url]


<p align="right">(<a href="#readme-top">back to top</a>)</p>

# Installation

## Dependencies

* python version 3.8 you can install in the link below
  https://www.python.org/downloads/release/python-380/ <br>
<br>
* you can install COCO dataset ("2017 Train images [118K/18GB]") in the link below <br>
https://cocodataset.org/#download <br>
<br>
* you can install the dataset json files in the link below <br>
https://github.com/jin-s13/COCO-WholeBody <br>

<br>
* cuDNN version 8.1 <br>
  CUDA version 11.2 <br>
  you can install by following the guide in the following video
  https://www.youtube.com/watch?v=hHWkvEcDBO0&t=304s
 
* opencv : pythonic distribution
  ```sh
  pip install opencv-python
  ```
* numpy 
  ```sh
  pip install numpy
  ```
* tensorflow 
  ```sh
  pip install tensorflow-gpu==2.10
  ```

## Finally 
* Clone the repo

* ```sh
  git clone https://gitlab.com/Dan1451/ashdod-1002-eye-tracking.git
  ```

<br>

# Activation
* In the folder `Eye_Tracking` activate `detect.py` or just in the folder of the project open cmd and <br>
enter the command "python path/to/your/detect.py"

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Contact

Project Link: [https://gitlab.com/Dan1451/ashdod-1002-eye-tracking](https://gitlab.com/Dan1451/ashdod-1002-eye-tracking)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[TensorFlow.py]:https://img.shields.io/badge/-Tensorflow-important
[TensorFlow-url]:https://www.tensorflow.org/?hl=he

[Python.py]:https://img.shields.io/badge/-Python-brightgreen
[Python-url]:https://www.python.org/

[Numpy.py]:https://img.shields.io/badge/-Numpy-orange
[Numpy-url]:https://numpy.org/

[opencv.py]:https://img.shields.io/badge/-OpenCV-blueviolet
[opencv-url]:https://opencv.org/
