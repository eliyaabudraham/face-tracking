from model import Model
import cv2 
from absl import logging
import tensorflow as tf
import utils
import face_orientation
import consts
import keyboard



def print_logo():
	print("""                                                                                                                                          
                                                                                                                                          
 /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$
|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/
 /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$
|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/
 /$$       /$$$$$$$$                                     /$$$$$$$$                               /$$                                 /$$  
| $$      | $$_____/                                    |__  $$__/                              | $$                                | $$  
| $$      | $$        /$$$$$$   /$$$$$$$  /$$$$$$          | $$     /$$$$$$   /$$$$$$   /$$$$$$$| $$   /$$  /$$$$$$   /$$$$$$       | $$  
|__/      | $$$$$    |____  $$ /$$_____/ /$$__  $$         | $$    /$$__  $$ |____  $$ /$$_____/| $$  /$$/ /$$__  $$ /$$__  $$      |__/  
 /$$      | $$__/     /$$$$$$$| $$      | $$$$$$$$         | $$   | $$  \__/  /$$$$$$$| $$      | $$$$$$/ | $$$$$$$$| $$  \__/       /$$  
| $$      | $$       /$$__  $$| $$      | $$_____/         | $$   | $$       /$$__  $$| $$      | $$_  $$ | $$_____/| $$            | $$  
| $$      | $$      |  $$$$$$$|  $$$$$$$|  $$$$$$$         | $$   | $$      |  $$$$$$$|  $$$$$$$| $$ \  $$|  $$$$$$$| $$            | $$  
|__/      |__/       \_______/ \_______/ \_______/         |__/   |__/       \_______/ \_______/|__/  \__/ \_______/|__/            |__/  
 /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$
|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/
 /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$ /$$$$
|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/|____/
                                                                                                                                          
                                                                                                                                          
                                                                                                                                          
                                                                                                                                          
                                                                                                                                          """)
def main():
	tf.config.run_functions_eagerly(True)
	print_logo()
	modelOption=input("Enter how to load model [ 's' - for saved model  |  'n' - for new model ]:  ")
	if modelOption =='n':
		model = Model()
	elif modelOption =='s':
		model = tf.keras.models.load_model('my_model_face_box')



	vid = cv2.VideoCapture(0)


	prev_x_center=-1
	prev_y_center=-1
	while True:
		_,img = vid.read()
		if img is None:
			logging.warning("Empty Frame")
			continue




		img_in = tf.expand_dims(img,0)
		img_in = utils.transform_images(img_in, consts.image_size[0])

		pred_bbox = model.predict(img_in)


		pred_bbox = [tf.reshape(x, (-1, tf.shape(x)[-1])) for x in pred_bbox]
		pred_bbox = tf.concat(pred_bbox, axis=0)
		boxes ,class_names, scores=utils.Deserializer(pred_bbox)
		x_center,y_center=utils.GetFaceCenter(boxes ,class_names, scores)
		if x_center!=-1 and y_center!=-1 and prev_x_center!=-1 and prev_y_center!=-1:
			face_orientation.calculating_face_orientation(x_center,y_center,prev_x_center,prev_y_center)
		prev_x_center=x_center
		prev_y_center=y_center

		if keyboard.is_pressed("q"):
			print("Exit.")
			break

	cv2.destroyAllWindows()





if __name__ == '__main__':
	main()
