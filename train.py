import tensorflow as tf
from tensorflow import keras
import cv2
import numpy as np
import utils
from pycocotools.coco import COCO
import consts
from load_weights import Load_weights
import matplotlib.pyplot as plt

COCO_URL = r"C:\Users\magshimim\Desktop\Magshimim-final_project/train.json"
IMAGE_FOLDER = r"C:\Users\magshimim\Desktop\Magshimim-final_project\train2017/"



coco = COCO(COCO_URL)
weight_file = "./weights/yolov3.weights"


def loss(labels, logits):
    total_loss = 0
    sum_objectness_loss = 0
    sum_bbox_loss = 0
    sum_class_loss = 0
    # Extract the objectness score
    labels = [labels[0], labels[1], labels[2]]
    for i in range(3):
        objectness = labels[i][..., 4:5]
        # Extract the bounding box
        bbox_true = labels[i][..., :4]
        bbox_pred = logits[i][..., :4]
        # Extract the class probability
        class_true = labels[i][..., 5:]
        class_pred = logits[i][..., 5:]

        # Create a mask for objects and background
        obj_mask = objectness[..., 0]
        noobj_mask = 1 - obj_mask

        # Compute the objectness loss
        obj_loss = obj_mask * tf.keras.losses.BinaryCrossentropy(from_logits=True)(y_true=objectness,
                                                                                   y_pred=logits[i][...,
                                                                                          4:5]) + noobj_mask * tf.keras.losses.BinaryCrossentropy(
            from_logits=True)(y_true=objectness, y_pred=logits[i][..., 4:5])
        # Compute the class loss
        class_loss = obj_mask * tf.keras.losses.CategoricalCrossentropy(from_logits=True)(class_true, class_pred)

        # Compute the bbox loss
        bbox_loss = obj_mask * tf.reduce_mean(tf.square(bbox_true - bbox_pred), axis=-1)

        # Compute the total loss by summing all losess
        sum_objectness_loss += tf.reduce_mean(obj_loss)
        sum_class_loss += tf.reduce_mean(class_loss)
        sum_bbox_loss += tf.reduce_mean(bbox_loss)
    total_loss += sum_objectness_loss + sum_class_loss + sum_bbox_loss
    return total_loss


def train(model):
    #model = Load_weights(model, weight_file)
    # Instantiate an optimizer
    optimizer = keras.optimizers.Adam(learning_rate=1e-4)
    train_loss = tf.keras.metrics.Mean(name='train_loss')

    # Instantiate a batch
    batch_size = 6

    image_ids = coco.getImgIds()
    dataset = tf.data.Dataset.from_tensor_slices(image_ids)
    tf.config.run_functions_eagerly(True)
    dataset = dataset.map(load_image_and_label_py).batch(batch_size)

    dataset = dataset.shuffle(buffer_size=8)

    print("done preprocess dataset")

    print("start training loop...")

    def train_step(images, labels):
        with tf.GradientTape() as tape:
            logits = model(images, training=True)
            total_loss = loss([labels[0], labels[1], labels[2]], logits)

        grads = tape.gradient(total_loss, model.trainable_variables)
        optimizer.apply_gradients(zip(grads, model.trainable_variables))

        train_loss(total_loss)

        return total_loss

    num_epochs = 1

    f = open("log.txt", 'w')
    for epoch in range(num_epochs):
        i = 0
        for images_batch, labels_batch in dataset:
            i += 1
            loss_value = train_step(images_batch, labels_batch)
            if i % 100 == 0 or i == 1:
                f.write(f"step : {i}  loss: {loss_value}\n")
                print(f"step : {i}  loss: {loss_value}")

        f.write(f"epoch : {epoch}  loss: {loss_value}\n")

        print(f"Epoch {epoch + 1}, Loss: {loss_value}")
    f.close()
    tf.keras.models.save_model(model, 'my_model')
    return model


def load_image_and_label_py(image_id):
    image, y_true_0, y_true_1, y_true_2 = tf.py_function(preprocess_coco_dataset, [image_id],
                                                         [tf.float32, tf.float32, tf.float32, tf.float32])
    return image, (y_true_0, y_true_1, y_true_2) # [52,26,13]


def preprocess_coco_dataset(image_id):
    tf.config.run_functions_eagerly(True)
    # Load the image

    image_id = int(image_id.numpy())

    # print(image_id.numpy())

    image_info = coco.loadImgs(image_id)[0]

    image_file = tf.strings.join([IMAGE_FOLDER, image_info['file_name']])

    image_file = str(image_file.numpy())
    image_file = image_file[2:-1]

    image = cv2.imread(image_file)

    # Resize the image to the specified size
    image = cv2.resize(image, consts.image_size)

    # normalize
    image = np.array(image, dtype=np.float32)
    image /= 255.0
    # Preprocess the labels
    ann_ids = coco.getAnnIds(imgIds=image_id)
    annotations = coco.loadAnns(ann_ids)
    label = [np.zeros((grid_size, grid_size, 3, 5 + consts.num_classes), dtype='float32') for grid_size in
             consts.full_grid_size]

    for annotation in annotations:
        # Get the category ID of the object
        category_id = annotation['category_id']

        # Get the bounding box coordinates and convert them to the YOLO format
        x, y, w, h = annotation['lefthand_box']
        x_center = x + w / 2
        y_center = y + h / 2

        # Scale the bounding box coordinates by the scaling factor
        scale_x = consts.image_size[0] / image_info['width']
        scale_y = consts.image_size[1] / image_info['height']
        x_center *= scale_x
        y_center *= scale_y
        w *= scale_x
        h *= scale_y

        # Clamp the coordinates to be within the bounds of the resized image size
        x_center = max(min(x_center, consts.image_size[0]), 0)
        y_center = max(min(y_center, consts.image_size[1]), 0)
        w = max(min(w, consts.image_size[0]), 0)
        h = max(min(h, consts.image_size[1]), 0)

        x_center = x_center / consts.image_size[0]
        y_center = y_center / consts.image_size[1]
        w = w / consts.image_size[0]
        h = h / consts.image_size[1]
        # Convert the scaled and clamped coordinates to the grid cell indices
        for l, grid_size in enumerate(consts.full_grid_size):
            x_grid = int(x_center / consts.image_size[0] * grid_size)
            y_grid = int(y_center / consts.image_size[1] * grid_size)
            if x_grid > grid_size:
                x_grid = grid_size - 1
            if y_grid > grid_size:
                y_grid = grid_size - 1

            # Set the label values for the corresponding grid cell and object category
            obj_score = 0
            if x_center != 0 or y_center != 0:
                obj_score = 1
            label[l][y_grid, x_grid, :, :5] = [x_center, y_center, w, h, obj_score]
            label[l][y_grid, x_grid, :, 5 + category_id] = obj_score


    return np.array(image), label[0], label[1], label[2]


