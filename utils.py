import tensorflow as tf
import numpy as np
import consts
import cv2



def transform_images(img, size):
    img = tf.image.resize(img, (size, size))
    img = img / 255
    return img

def Deserializer(pred):

    center_x,center_y,width,height,confidence,classes = tf.split(pred,[1,1,1,1,1,-1], axis=-1)

    top_left_x=(center_x-width/2.)/ 416
    top_left_y = (center_y - height / 2.0)/416.0
    bottom_right_x = (center_x + width / 2.0)/416.0
    bottom_right_y = (center_y + height / 2.0)/416.0
    boxes = tf.concat([top_left_y,top_left_x,bottom_right_y,bottom_right_x],axis=-1)
    scores = confidence*classes
    scores = np.array(scores)

    scores = scores.max(axis=-1)
    class_index = np.argmax(classes, axis=-1)

    final_indexes = tf.image.non_max_suppression(boxes,scores, max_output_size= 20)
    final_indexes = np.array(final_indexes)
    class_names = class_index[final_indexes]
    boxes = np.array(boxes)
    scores = np.array(scores)
    class_names = np.array(class_names)
    boxes = boxes[final_indexes,:]

    scores = scores[final_indexes]
    boxes = boxes*416

    return boxes ,class_names, scores



def GetFaceCenter(boxes, class_names,scores):
    data = np.concatenate([boxes,scores[:,np.newaxis],class_names[:,np.newaxis]],axis=-1)
    data = data[np.logical_and(data[:, 0] >= 0, data[:, 0] <= 416)]
    data = data[np.logical_and(data[:, 1] >= 0, data[:, 1] <= 416)]
    data = data[np.logical_and(data[:, 2] >= 0, data[:, 2] <= 416)]
    data = data[np.logical_and(data[:, 3] >= 0, data[:,3] <= 416)]
    data = data[data[:,4]>0.7]
    x_center=-1
    y_center=-1
    for i,row in enumerate(data):

        x_center = (int(row[3]) + int(row[1])) / 2
        y_center = (int(row[0])+ int(row[2])) / 2

    return x_center, y_center


